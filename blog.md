# Lessons Learned Building a Van Laarhoven Lens Library

## What are Lenses

The original motivation for lenses comes from wanting a better syntax for modifying nested records

Consider for example the following case classes. 
```scala
case class Person(name: String, address: Address)
case class Address(
    houseName: String, 
    streetName: String)

val jane = Person("Jane",
    Address("22B", "Baker Street")
  )
```
We have a Record containing data about a `Person`, this includes a Record representing an `Address`, which contains a `streetName`, that has a type `String`.

![Person -> Address -> StreetName](imgs/person-address-street.png)




